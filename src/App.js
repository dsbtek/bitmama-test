import { useEffect, useState, useLayoutEffect } from 'react';
import './App.css';
import UserInput from './components/form';
import ListOfUsers from './components/listOfUsers';
import LoginUser from './components/LogInUsers';

function App() {
  const store = localStorage;
  const [user, setUser] = useState({"userName":"", "status":""})
  const [userList] = useState([])
  const [active, setActive] = useState(false)
  let userKeys = Object.keys(store)
  let userValues = Object.values(store)
  const getUsers = () => {
    for (let i = 0; i < userKeys.length; i++){
      userList.push({"userName":userKeys[i], "status":userValues[i]})
    }
    
   return userList;
}
  const logIn =() => {
    store.setItem(user.userName, user.status)
    getUsers()
  }

  const logOut = () =>{
    store.removeItem(user.userName)
    setActive(false)
    getUsers()
}

const hasFocus = () => {
  if (document.hasFocus() !==  true) {
    setUser(
      prevState => ({
        ...prevState,
        status: "idle"
    })
    )
    getUsers();
  } else {
    getUsers();
  }
}
setInterval(hasFocus, 6000)
// setTimeout(hasFocus, 600);

useEffect(() => {
  const loggedInUser = store.getItem(user);
  if (loggedInUser) {
    const foundUser = JSON.parse(loggedInUser);
    setUser(foundUser);
    setActive(true)
  }
}, [store, user]);

// useLayoutEffect(() => {
//     if (userKeys.includes(user.userName) === true){
//       setActive(true)
//     } else{setActive(false)}
//   }, [active, user.userName, userKeys]);
 
  return (
    <div className="App">
      <header className="App-header">
      {active?
      <div>
        <LoginUser user={user} logOut={logOut}/>
       {userList.map((statux) => { 
         return <ListOfUsers status={statux} store={store} getUsers={getUsers}/>
       })}
      </div>
      :
        <UserInput logIn = {logIn} user = {user} setUser = {setUser} />
      }
      </header>
    </div>
  );
}
export default App;
