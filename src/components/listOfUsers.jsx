// import Users from "../services/service";


const ListOfUsers = (props) => {
    const  {status, store, getUsers} = props

    const logOutAUser = () =>{
        if (Object.keys(store).includes(status.userName) === true){
            store.removeItem(status.userName)
            getUsers()
        }
      }
    // console.log(status)
    return(
        <ul>
            <li key={status.userName}>
                {status.userName} 
                <p className={status.status === "idle" ? "idle":"active"}>{status.status}</p> <button className="btn" onClick={logOutAUser}>Log Out</button></li> 
        </ul>
    )
}
export default ListOfUsers;