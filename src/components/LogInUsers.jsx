const LoginUser = (props) => {
    const {user, logOut} = props
    
    const handleLogOut = e => {
       return logOut(user.userName)
    }
    const url = "http://localhost:3000/"
    const handleNewLogInUser = () =>{
        var win = window.open(url, '_blank');
  win.focus()
    }

    return(
        <div>
            <h2>Welcome: {user.userName}</h2> 
            <button className="btn" onClick={handleLogOut}>Log Out</button>
            <button className="btn" onClick={handleNewLogInUser}>Log In as different User</button>
        </div>
    )
}
export default LoginUser;