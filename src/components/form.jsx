
const UserInput = (props) => {
    let {user, setUser, logIn} = props;
    const handleChange = e => {
        setUser(prevState => ({
                ...prevState,
                [e.target.id]: e.target.value
            }));
    };
    const handleLogin = e => {
        e.preventDefault();
        setUser(prevState => ({
            ...prevState,
            status: 'active'
        }))
        logIn(user.userName, user.status)
    }
    return(
        <form className="wrap-form" onSubmit={handleLogin}>
            <label>User Name</label>
            <input
                id="userName"
                type="text" 
                name="userName" 
                placeholder="Enter User name" 
                value={user.userName} 
                onChange = {handleChange}
        />
            <input type="submit" value="sign in" />
        </form>
    )
}
export default UserInput;