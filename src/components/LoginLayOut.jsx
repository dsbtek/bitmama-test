import ListOfUsers from "./listOfUsers";
import LoginUser from "./LogInUsers";

const LogInLayOut = () => {
    return(
        <div>
            <LoginUser />
            <ListOfUsers />
        </div>
    )
}
export default LogInLayOut;